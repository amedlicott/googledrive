package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for CloudStorage:
{
  "type": "object",
  "properties": {
    "shared": {
      "type": "integer",
      "format": "int32"
    },
    "total": {
      "type": "integer",
      "format": "int32"
    },
    "used": {
      "type": "integer",
      "format": "int32"
    }
  }
}
*/

public class CloudStorage {

	@Size(max=1)
	private Integer shared;

	@Size(max=1)
	private Integer total;

	@Size(max=1)
	private Integer used;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    
	    
	    
	}
	public Integer getShared() {
		return shared;
	}
	
	public void setShared(Integer shared) {
		this.shared = shared;
	}
	public Integer getTotal() {
		return total;
	}
	
	public void setTotal(Integer total) {
		this.total = total;
	}
	public Integer getUsed() {
		return used;
	}
	
	public void setUsed(Integer used) {
		this.used = used;
	}
}