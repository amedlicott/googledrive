package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for Pong:
{
  "type": "object",
  "properties": {
    "dateTime": {
      "type": "string"
    },
    "endpoint": {
      "type": "string"
    }
  }
}
*/

public class Pong {

	@Size(max=1)
	private String dateTime;

	@Size(max=1)
	private String endpoint;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    dateTime = null;
	    endpoint = null;
	}
	public String getDateTime() {
		return dateTime;
	}
	
	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}
	public String getEndpoint() {
		return endpoint;
	}
	
	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}
}