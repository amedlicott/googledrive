package com.digitalml.rest.resources.codegentest.resource;
        	
import java.lang.IllegalArgumentException;
import java.security.AccessControlException;

import javax.servlet.http.HttpServletRequest;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import io.dropwizard.jersey.PATCH;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.PathParam;
import javax.ws.rs.FormParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.lang.Object;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
	
// Customer specific imports

// Service specific imports
import com.digitalml.rest.resources.codegentest.service.GoogledriveService;
	
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.DeleteFolderByIdReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.DeleteFolderByIdReturnDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.DeleteFolderByIdInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.GetFoldersContentsReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.GetFoldersContentsReturnDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.GetFoldersContentsInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.GetFilesLinksReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.GetFilesLinksReturnDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.GetFilesLinksInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.GetSearchReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.GetSearchReturnDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.GetSearchInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.CreateFileCopyReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.CreateFileCopyReturnDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.CreateFileCopyInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.CreateFolderCopyReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.CreateFolderCopyReturnDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.CreateFolderCopyInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.GetFilesReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.GetFilesReturnDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.GetFilesInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.CreateFileReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.CreateFileReturnDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.CreateFileInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.DeleteFilesReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.DeleteFilesReturnDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.DeleteFilesInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.CreateFolderReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.CreateFolderReturnDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.CreateFolderInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.DeleteFoldersReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.DeleteFoldersReturnDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.DeleteFoldersInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.GetPingReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.GetPingReturnDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.GetPingInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.GetStorageReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.GetStorageReturnDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.GetStorageInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.GetFoldersContentsReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.GetFoldersContentsReturnDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.GetFoldersContentsInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.GetFoldersMetadataReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.GetFoldersMetadataReturnDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.GetFoldersMetadataInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.UpdateFoldersMetadataReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.UpdateFoldersMetadataReturnDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.UpdateFoldersMetadataInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.GetFilesLinksReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.GetFilesLinksReturnDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.GetFilesLinksInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.UpdateFilesMetadataPropertiesReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.UpdateFilesMetadataPropertiesReturnDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.UpdateFilesMetadataPropertiesInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.CreateFileCopyReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.CreateFileCopyReturnDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.CreateFileCopyInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.GetFoldersMetadataReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.GetFoldersMetadataReturnDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.GetFoldersMetadataInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.UpdateFoldersMetadataReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.UpdateFoldersMetadataReturnDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.UpdateFoldersMetadataInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.CreateFolderCopyReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.CreateFolderCopyReturnDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.CreateFolderCopyInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.GetFilesMetadataReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.GetFilesMetadataReturnDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.GetFilesMetadataInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.UpdateFilesMetadataReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.UpdateFilesMetadataReturnDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.UpdateFilesMetadataInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.GetFileByIdReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.GetFileByIdReturnDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.GetFileByIdInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.DeleteFileByIdReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.DeleteFileByIdReturnDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.DeleteFileByIdInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.GetFilesMetadataReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.GetFilesMetadataReturnDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.GetFilesMetadataInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.UpdateFilesMetadataReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.UpdateFilesMetadataReturnDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.UpdateFilesMetadataInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.UpdateFilesMetadataPropertiesReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.UpdateFilesMetadataPropertiesReturnDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.UpdateFilesMetadataPropertiesInputParametersDTO;
	
import com.digitalml.rest.resources.codegentest.*;
	
	/**
	 * Service: googledrive
	 * 120
	 *
	 * @author admin
	 * @version api-v2
	 *
	 */
	
	@Path("http://console.cloud-elements.com/elements/api-v2/hubs/documents")
		@Produces({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
	public class GoogledriveResource {
		
	private static final Logger LOGGER = LoggerFactory.getLogger(GoogledriveResource.class);
	
	@Context
	private SecurityContext securityContext;

	@Context
	private javax.ws.rs.core.Request request;

	@Context
	private HttpServletRequest httpRequest;

	private GoogledriveService delegateService;

	private String implementationClass = "com.digitalml.rest.resources.codegentest.service.Googledrive.GoogledriveServiceDefaultImpl";

	public void setImplementationClass(String className) {
		implementationClass = className;
	}

	public void setImplementationClass(Class clazz) {
		if (clazz == null)
			throw new IllegalArgumentException("Parameter clazz cannot be null");

		implementationClass = clazz.getName();
	}
		
	private GoogledriveService getCurrentImplementation() {

		Object object = null;
		try {
			Class c = Class.forName(implementationClass, true, Thread.currentThread().getContextClassLoader());
			object = c.newInstance();

		} catch (ClassNotFoundException exc) {
			LOGGER.error(implementationClass + " not found");
			return null;

		} catch (IllegalAccessException exc) {
			LOGGER.error("Cannot access " + implementationClass);
			return null;

		} catch (InstantiationException exc) {
			LOGGER.error("Cannot instantiate " + implementationClass);
			return null;
		}

		if (!(object instanceof GoogledriveService)) {
			LOGGER.error(implementationClass + " is not an instance of " + GoogledriveService.class.getName());
			return null;
		}

		return (GoogledriveService)object;
	}
	
	{
		delegateService = getCurrentImplementation();
	}
	
	public void setSecurityContext(SecurityContext securityContext) {
		this.securityContext = securityContext;
	}


	/**
	Method: deleteFolderById

	Non-functional requirements:
	*/
	
	@DELETE
	@Path("/folders/{id}")
	public javax.ws.rs.core.Response deleteFolderById(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("id")@NotEmpty String id,
		@QueryParam("emptyTrash") boolean emptyTrash) {

		DeleteFolderByIdInputParametersDTO inputs = new GoogledriveService.DeleteFolderByIdInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setId(id);
		inputs.setEmptyTrash(emptyTrash);
	
		try {
			DeleteFolderByIdReturnDTO returnValue = delegateService.deleteFolderById(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: getFoldersContents

	Non-functional requirements:
	*/
	
	@GET
	@Path("/folders/{id}/contents")
	public javax.ws.rs.core.Response getFoldersContents(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("id")@NotEmpty String id,
		@QueryParam("fetchTags") boolean fetchTags,
		@QueryParam("pageSize") Integer pageSize,
		@QueryParam("page") Integer page) {

		GetFoldersContentsInputParametersDTO inputs = new GoogledriveService.GetFoldersContentsInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setId(id);
		inputs.setFetchTags(fetchTags);
		inputs.setPageSize(pageSize);
		inputs.setPage(page);
	
		try {
			GetFoldersContentsReturnDTO returnValue = delegateService.getFoldersContents(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: getFilesLinks

	Non-functional requirements:
	*/
	
	@GET
	@Path("/files/links")
	public javax.ws.rs.core.Response getFilesLinks(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@QueryParam("path")@NotEmpty String path) {

		GetFilesLinksInputParametersDTO inputs = new GoogledriveService.GetFilesLinksInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setPath(path);
	
		try {
			GetFilesLinksReturnDTO returnValue = delegateService.getFilesLinks(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: getSearch

	Non-functional requirements:
	*/
	
	@GET
	@Path("/search")
	public javax.ws.rs.core.Response getSearch(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@QueryParam("path") String path,
		@QueryParam("text") String text,
		@QueryParam("startDate") String startDate,
		@QueryParam("endDate") String endDate,
		 List<String> tags[],
		@QueryParam("pageSize") Integer pageSize,
		@QueryParam("page") Integer page) {

		GetSearchInputParametersDTO inputs = new GoogledriveService.GetSearchInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setPath(path);
		inputs.setText(text);
		inputs.setStartDate(startDate);
		inputs.setEndDate(endDate);
		inputs.setTags(tags[]);
		inputs.setPageSize(pageSize);
		inputs.setPage(page);
	
		try {
			GetSearchReturnDTO returnValue = delegateService.getSearch(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createFileCopy

	Non-functional requirements:
	*/
	
	@POST
	@Path("/files/{id}/copy")
	public javax.ws.rs.core.Response createFileCopy(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("id")@NotEmpty String id,
		 com.digitalml.rest.resources.codegentest.CloudFile body) {

		CreateFileCopyInputParametersDTO inputs = new GoogledriveService.CreateFileCopyInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setId(id);
		inputs.setBody(body);
	
		try {
			CreateFileCopyReturnDTO returnValue = delegateService.createFileCopy(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createFolderCopy

	Non-functional requirements:
	*/
	
	@POST
	@Path("/folders/{id}/copy")
	public javax.ws.rs.core.Response createFolderCopy(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("id")@NotEmpty String id,
		 com.digitalml.rest.resources.codegentest.CloudFile body) {

		CreateFolderCopyInputParametersDTO inputs = new GoogledriveService.CreateFolderCopyInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setId(id);
		inputs.setBody(body);
	
		try {
			CreateFolderCopyReturnDTO returnValue = delegateService.createFolderCopy(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: getFiles

	Non-functional requirements:
	*/
	
	@GET
	@Path("/files")
	public javax.ws.rs.core.Response getFiles(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@QueryParam("path")@NotEmpty String path) {

		GetFilesInputParametersDTO inputs = new GoogledriveService.GetFilesInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setPath(path);
	
		try {
			GetFilesReturnDTO returnValue = delegateService.getFiles(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createFile

	Non-functional requirements:
	*/
	
	@POST
	@Path("/files")
	public javax.ws.rs.core.Response createFile(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("file") String file,
		@QueryParam("path")@NotEmpty String path,
		@QueryParam("size") Integer size,
		 List<String> tags[],
		@QueryParam("description") String description,
		@QueryParam("overwrite") boolean overwrite) {

		CreateFileInputParametersDTO inputs = new GoogledriveService.CreateFileInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setFile(file);
		inputs.setPath(path);
		inputs.setSize(size);
		inputs.setTags(tags[]);
		inputs.setDescription(description);
		inputs.setOverwrite(overwrite);
	
		try {
			CreateFileReturnDTO returnValue = delegateService.createFile(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: deleteFiles

	Non-functional requirements:
	*/
	
	@DELETE
	@Path("/files")
	public javax.ws.rs.core.Response deleteFiles(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@QueryParam("path")@NotEmpty String path,
		@QueryParam("emptyTrash") boolean emptyTrash) {

		DeleteFilesInputParametersDTO inputs = new GoogledriveService.DeleteFilesInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setPath(path);
		inputs.setEmptyTrash(emptyTrash);
	
		try {
			DeleteFilesReturnDTO returnValue = delegateService.deleteFiles(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createFolder

	Non-functional requirements:
	*/
	
	@POST
	@Path("/folders")
	public javax.ws.rs.core.Response createFolder(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		 com.digitalml.rest.resources.codegentest.CloudFile body) {

		CreateFolderInputParametersDTO inputs = new GoogledriveService.CreateFolderInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setBody(body);
	
		try {
			CreateFolderReturnDTO returnValue = delegateService.createFolder(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: deleteFolders

	Non-functional requirements:
	*/
	
	@DELETE
	@Path("/folders")
	public javax.ws.rs.core.Response deleteFolders(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@QueryParam("path")@NotEmpty String path,
		@QueryParam("emptyTrash") boolean emptyTrash) {

		DeleteFoldersInputParametersDTO inputs = new GoogledriveService.DeleteFoldersInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setPath(path);
		inputs.setEmptyTrash(emptyTrash);
	
		try {
			DeleteFoldersReturnDTO returnValue = delegateService.deleteFolders(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: getPing

	Non-functional requirements:
	*/
	
	@GET
	@Path("/ping")
	public javax.ws.rs.core.Response getPing(
		@QueryParam("Authorization")@NotEmpty String Authorization) {

		GetPingInputParametersDTO inputs = new GoogledriveService.GetPingInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
	
		try {
			GetPingReturnDTO returnValue = delegateService.getPing(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: getStorage

	Non-functional requirements:
	*/
	
	@GET
	@Path("/storage")
	public javax.ws.rs.core.Response getStorage(
		@QueryParam("Authorization")@NotEmpty String Authorization) {

		GetStorageInputParametersDTO inputs = new GoogledriveService.GetStorageInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
	
		try {
			GetStorageReturnDTO returnValue = delegateService.getStorage(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: getFoldersContents

	Non-functional requirements:
	*/
	
	@GET
	@Path("/folders/contents")
	public javax.ws.rs.core.Response getFoldersContents(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@QueryParam("path")@NotEmpty String path,
		@QueryParam("fetchTags") boolean fetchTags,
		@QueryParam("pageSize") Integer pageSize,
		@QueryParam("page") Integer page) {

		GetFoldersContentsInputParametersDTO inputs = new GoogledriveService.GetFoldersContentsInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setPath(path);
		inputs.setFetchTags(fetchTags);
		inputs.setPageSize(pageSize);
		inputs.setPage(page);
	
		try {
			GetFoldersContentsReturnDTO returnValue = delegateService.getFoldersContents(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: getFoldersMetadata

	Non-functional requirements:
	*/
	
	@GET
	@Path("/folders/metadata")
	public javax.ws.rs.core.Response getFoldersMetadata(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@QueryParam("path")@NotEmpty String path) {

		GetFoldersMetadataInputParametersDTO inputs = new GoogledriveService.GetFoldersMetadataInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setPath(path);
	
		try {
			GetFoldersMetadataReturnDTO returnValue = delegateService.getFoldersMetadata(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: updateFoldersMetadata

	Non-functional requirements:
	*/
	
	@PATCH
	@Path("/folders/metadata")
	public javax.ws.rs.core.Response updateFoldersMetadata(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@QueryParam("path")@NotEmpty String path,
		 com.digitalml.rest.resources.codegentest.Metadata body) {

		UpdateFoldersMetadataInputParametersDTO inputs = new GoogledriveService.UpdateFoldersMetadataInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setPath(path);
		inputs.setBody(body);
	
		try {
			UpdateFoldersMetadataReturnDTO returnValue = delegateService.updateFoldersMetadata(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: getFilesLinks

	Non-functional requirements:
	*/
	
	@GET
	@Path("/files/{id}/links")
	public javax.ws.rs.core.Response getFilesLinks(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("id")@NotEmpty String id) {

		GetFilesLinksInputParametersDTO inputs = new GoogledriveService.GetFilesLinksInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setId(id);
	
		try {
			GetFilesLinksReturnDTO returnValue = delegateService.getFilesLinks(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: updateFilesMetadataProperties

	Non-functional requirements:
	*/
	
	@PATCH
	@Path("/files/{id}/metadata/properties")
	public javax.ws.rs.core.Response updateFilesMetadataProperties(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("id")@NotEmpty String id,
		 com.digitalml.rest.resources.codegentest.CustomProperties body) {

		UpdateFilesMetadataPropertiesInputParametersDTO inputs = new GoogledriveService.UpdateFilesMetadataPropertiesInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setId(id);
		inputs.setBody(body);
	
		try {
			UpdateFilesMetadataPropertiesReturnDTO returnValue = delegateService.updateFilesMetadataProperties(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createFileCopy

	Non-functional requirements:
	*/
	
	@POST
	@Path("/files/copy")
	public javax.ws.rs.core.Response createFileCopy(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@QueryParam("path")@NotEmpty String path,
		 com.digitalml.rest.resources.codegentest.CloudFile body) {

		CreateFileCopyInputParametersDTO inputs = new GoogledriveService.CreateFileCopyInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setPath(path);
		inputs.setBody(body);
	
		try {
			CreateFileCopyReturnDTO returnValue = delegateService.createFileCopy(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: getFoldersMetadata

	Non-functional requirements:
	*/
	
	@GET
	@Path("/folders/{id}/metadata")
	public javax.ws.rs.core.Response getFoldersMetadata(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("id")@NotEmpty String id) {

		GetFoldersMetadataInputParametersDTO inputs = new GoogledriveService.GetFoldersMetadataInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setId(id);
	
		try {
			GetFoldersMetadataReturnDTO returnValue = delegateService.getFoldersMetadata(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: updateFoldersMetadata

	Non-functional requirements:
	*/
	
	@PATCH
	@Path("/folders/{id}/metadata")
	public javax.ws.rs.core.Response updateFoldersMetadata(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("id")@NotEmpty String id,
		 com.digitalml.rest.resources.codegentest.Metadata body) {

		UpdateFoldersMetadataInputParametersDTO inputs = new GoogledriveService.UpdateFoldersMetadataInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setId(id);
		inputs.setBody(body);
	
		try {
			UpdateFoldersMetadataReturnDTO returnValue = delegateService.updateFoldersMetadata(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createFolderCopy

	Non-functional requirements:
	*/
	
	@POST
	@Path("/folders/copy")
	public javax.ws.rs.core.Response createFolderCopy(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@QueryParam("path")@NotEmpty String path,
		 com.digitalml.rest.resources.codegentest.CloudFile body) {

		CreateFolderCopyInputParametersDTO inputs = new GoogledriveService.CreateFolderCopyInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setPath(path);
		inputs.setBody(body);
	
		try {
			CreateFolderCopyReturnDTO returnValue = delegateService.createFolderCopy(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: getFilesMetadata

	Non-functional requirements:
	*/
	
	@GET
	@Path("/files/metadata")
	public javax.ws.rs.core.Response getFilesMetadata(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@QueryParam("path")@NotEmpty String path) {

		GetFilesMetadataInputParametersDTO inputs = new GoogledriveService.GetFilesMetadataInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setPath(path);
	
		try {
			GetFilesMetadataReturnDTO returnValue = delegateService.getFilesMetadata(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: updateFilesMetadata

	Non-functional requirements:
	*/
	
	@PATCH
	@Path("/files/metadata")
	public javax.ws.rs.core.Response updateFilesMetadata(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@QueryParam("path")@NotEmpty String path,
		 com.digitalml.rest.resources.codegentest.Metadata body) {

		UpdateFilesMetadataInputParametersDTO inputs = new GoogledriveService.UpdateFilesMetadataInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setPath(path);
		inputs.setBody(body);
	
		try {
			UpdateFilesMetadataReturnDTO returnValue = delegateService.updateFilesMetadata(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: getFileById

	Non-functional requirements:
	*/
	
	@GET
	@Path("/files/{id}")
	public javax.ws.rs.core.Response getFileById(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("id")@NotEmpty String id) {

		GetFileByIdInputParametersDTO inputs = new GoogledriveService.GetFileByIdInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setId(id);
	
		try {
			GetFileByIdReturnDTO returnValue = delegateService.getFileById(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: deleteFileById

	Non-functional requirements:
	*/
	
	@DELETE
	@Path("/files/{id}")
	public javax.ws.rs.core.Response deleteFileById(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("id")@NotEmpty String id,
		@QueryParam("emptyTrash") boolean emptyTrash) {

		DeleteFileByIdInputParametersDTO inputs = new GoogledriveService.DeleteFileByIdInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setId(id);
		inputs.setEmptyTrash(emptyTrash);
	
		try {
			DeleteFileByIdReturnDTO returnValue = delegateService.deleteFileById(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: getFilesMetadata

	Non-functional requirements:
	*/
	
	@GET
	@Path("/files/{id}/metadata")
	public javax.ws.rs.core.Response getFilesMetadata(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("id")@NotEmpty String id) {

		GetFilesMetadataInputParametersDTO inputs = new GoogledriveService.GetFilesMetadataInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setId(id);
	
		try {
			GetFilesMetadataReturnDTO returnValue = delegateService.getFilesMetadata(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: updateFilesMetadata

	Non-functional requirements:
	*/
	
	@PATCH
	@Path("/files/{id}/metadata")
	public javax.ws.rs.core.Response updateFilesMetadata(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("id")@NotEmpty String id,
		 com.digitalml.rest.resources.codegentest.Metadata body) {

		UpdateFilesMetadataInputParametersDTO inputs = new GoogledriveService.UpdateFilesMetadataInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setId(id);
		inputs.setBody(body);
	
		try {
			UpdateFilesMetadataReturnDTO returnValue = delegateService.updateFilesMetadata(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: updateFilesMetadataProperties

	Non-functional requirements:
	*/
	
	@PATCH
	@Path("/files/metadata/properties")
	public javax.ws.rs.core.Response updateFilesMetadataProperties(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@QueryParam("path")@NotEmpty String path,
		 com.digitalml.rest.resources.codegentest.CustomProperties body) {

		UpdateFilesMetadataPropertiesInputParametersDTO inputs = new GoogledriveService.UpdateFilesMetadataPropertiesInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setPath(path);
		inputs.setBody(body);
	
		try {
			UpdateFilesMetadataPropertiesReturnDTO returnValue = delegateService.updateFilesMetadataProperties(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
}