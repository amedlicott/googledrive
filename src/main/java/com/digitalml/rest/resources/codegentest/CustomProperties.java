package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for CustomProperties:
{
  "type": "object",
  "properties": {
    "properties": {
      "type": "object",
      "properties": {
        "customKey": {
          "type": "string"
        }
      }
    }
  }
}
*/

public class CustomProperties {

	@Size(max=1)
	private com.digitalml.rest.resources.codegentest.Properties properties;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    properties = null;
	}
	public com.digitalml.rest.resources.codegentest.Properties getProperties() {
		return properties;
	}
	
	public void setProperties(com.digitalml.rest.resources.codegentest.Properties properties) {
		this.properties = properties;
	}
}