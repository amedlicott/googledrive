package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for CloudLink:
{
  "type": "object",
  "properties": {
    "cloudElementsLink": {
      "type": "string"
    },
    "expires": {
      "type": "string"
    },
    "providerLink": {
      "type": "string"
    },
    "providerViewLink": {
      "type": "string"
    }
  }
}
*/

public class CloudLink {

	@Size(max=1)
	private String cloudElementsLink;

	@Size(max=1)
	private String expires;

	@Size(max=1)
	private String providerLink;

	@Size(max=1)
	private String providerViewLink;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    cloudElementsLink = null;
	    expires = null;
	    providerLink = null;
	    providerViewLink = null;
	}
	public String getCloudElementsLink() {
		return cloudElementsLink;
	}
	
	public void setCloudElementsLink(String cloudElementsLink) {
		this.cloudElementsLink = cloudElementsLink;
	}
	public String getExpires() {
		return expires;
	}
	
	public void setExpires(String expires) {
		this.expires = expires;
	}
	public String getProviderLink() {
		return providerLink;
	}
	
	public void setProviderLink(String providerLink) {
		this.providerLink = providerLink;
	}
	public String getProviderViewLink() {
		return providerViewLink;
	}
	
	public void setProviderViewLink(String providerViewLink) {
		this.providerViewLink = providerViewLink;
	}
}