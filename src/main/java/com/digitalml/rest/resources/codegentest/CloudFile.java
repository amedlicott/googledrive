package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for CloudFile:
{
  "required": [
    "path",
    "tags"
  ],
  "type": "object",
  "properties": {
    "createdDate": {
      "type": "string"
    },
    "directory": {
      "type": "boolean"
    },
    "id": {
      "type": "string"
    },
    "modifiedDate": {
      "type": "string"
    },
    "name": {
      "type": "string"
    },
    "path": {
      "type": "string"
    },
    "size": {
      "type": "integer",
      "format": "int32"
    },
    "tags": {
      "minItems": 1,
      "type": "array",
      "items": {
        "type": "string"
      }
    }
  }
}
*/

public class CloudFile {

	@Size(max=1)
	private String createdDate;

	@Size(max=1)
	private boolean directory;

	@Size(max=1)
	private String id;

	@Size(max=1)
	private String modifiedDate;

	@Size(max=1)
	private String name;

	@Size(max=1)
	@NotNull
	private String path;

	@Size(max=1)
	private Integer size;

	private List<String> tags;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    createdDate = null;
	    
	    id = null;
	    modifiedDate = null;
	    name = null;
	    path = org.apache.commons.lang3.StringUtils.EMPTY;
	    
	    tags = new ArrayList<String>();
	}
	public String getCreatedDate() {
		return createdDate;
	}
	
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public boolean getDirectory() {
		return directory;
	}
	
	public void setDirectory(boolean directory) {
		this.directory = directory;
	}
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	public String getModifiedDate() {
		return modifiedDate;
	}
	
	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	public String getPath() {
		return path;
	}
	
	public void setPath(String path) {
		this.path = path;
	}
	public Integer getSize() {
		return size;
	}
	
	public void setSize(Integer size) {
		this.size = size;
	}
	public List<String> getTags() {
		return tags;
	}
	
	public void setTags(List<String> tags) {
		this.tags = tags;
	}
}