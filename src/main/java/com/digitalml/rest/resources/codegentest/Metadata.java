package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for Metadata:
{
  "type": "object",
  "properties": {
    "path": {
      "type": "string"
    },
    "tags": {
      "type": "array",
      "items": {
        "type": "string"
      }
    }
  }
}
*/

public class Metadata {

	@Size(max=1)
	private String path;

	@Size(max=1)
	private List<String> tags;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    path = null;
	    tags = new ArrayList<String>();
	}
	public String getPath() {
		return path;
	}
	
	public void setPath(String path) {
		this.path = path;
	}
	public List<String> getTags() {
		return tags;
	}
	
	public void setTags(List<String> tags) {
		this.tags = tags;
	}
}