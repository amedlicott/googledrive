package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Googledrive.GoogledriveServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.CreateFolderCopyInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.CreateFolderCopyReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class CreateFolderCopyTests {

	@Test
	public void testOperationCreateFolderCopyBasicMapping()  {
		GoogledriveServiceDefaultImpl serviceDefaultImpl = new GoogledriveServiceDefaultImpl();
		CreateFolderCopyInputParametersDTO inputs = new CreateFolderCopyInputParametersDTO();
		inputs.setAuthorization(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setId(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setBody(new CloudFile());
		CreateFolderCopyReturnDTO returnValue = serviceDefaultImpl.createFolderCopy(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponseWrapper200());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}