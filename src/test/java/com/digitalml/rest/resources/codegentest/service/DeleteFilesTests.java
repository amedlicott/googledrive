package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Googledrive.GoogledriveServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.DeleteFilesInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.DeleteFilesReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class DeleteFilesTests {

	@Test
	public void testOperationDeleteFilesBasicMapping()  {
		GoogledriveServiceDefaultImpl serviceDefaultImpl = new GoogledriveServiceDefaultImpl();
		DeleteFilesInputParametersDTO inputs = new DeleteFilesInputParametersDTO();
		inputs.setAuthorization(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setPath(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setEmptyTrash(false);
		DeleteFilesReturnDTO returnValue = serviceDefaultImpl.deleteFiles(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}