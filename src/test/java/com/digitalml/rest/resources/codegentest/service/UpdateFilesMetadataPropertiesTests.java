package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Googledrive.GoogledriveServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.UpdateFilesMetadataPropertiesInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.UpdateFilesMetadataPropertiesReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class UpdateFilesMetadataPropertiesTests {

	@Test
	public void testOperationUpdateFilesMetadataPropertiesBasicMapping()  {
		GoogledriveServiceDefaultImpl serviceDefaultImpl = new GoogledriveServiceDefaultImpl();
		UpdateFilesMetadataPropertiesInputParametersDTO inputs = new UpdateFilesMetadataPropertiesInputParametersDTO();
		inputs.setAuthorization(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setId(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setBody(new CustomProperties());
		UpdateFilesMetadataPropertiesReturnDTO returnValue = serviceDefaultImpl.updateFilesMetadataProperties(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponseWrapper200());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}