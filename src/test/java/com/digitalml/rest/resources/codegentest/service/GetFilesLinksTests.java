package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Googledrive.GoogledriveServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.GetFilesLinksInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.GetFilesLinksReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class GetFilesLinksTests {

	@Test
	public void testOperationGetFilesLinksBasicMapping()  {
		GoogledriveServiceDefaultImpl serviceDefaultImpl = new GoogledriveServiceDefaultImpl();
		GetFilesLinksInputParametersDTO inputs = new GetFilesLinksInputParametersDTO();
		inputs.setAuthorization(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setPath(org.apache.commons.lang3.StringUtils.EMPTY);
		GetFilesLinksReturnDTO returnValue = serviceDefaultImpl.getFilesLinks(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponseWrapper200());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}