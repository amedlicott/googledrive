package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Googledrive.GoogledriveServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.GetSearchInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.GetSearchReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class GetSearchTests {

	@Test
	public void testOperationGetSearchBasicMapping()  {
		GoogledriveServiceDefaultImpl serviceDefaultImpl = new GoogledriveServiceDefaultImpl();
		GetSearchInputParametersDTO inputs = new GetSearchInputParametersDTO();
		inputs.setAuthorization(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setPath(null);
		inputs.setText(null);
		inputs.setStartDate(null);
		inputs.setEndDate(null);
		inputs.setTags(new ArrayList&lt;String&gt;());
		inputs.setPageSize(0);
		inputs.setPage(0);
		GetSearchReturnDTO returnValue = serviceDefaultImpl.getSearch(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}