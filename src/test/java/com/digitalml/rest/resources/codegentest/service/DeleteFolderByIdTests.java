package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Googledrive.GoogledriveServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.DeleteFolderByIdInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.DeleteFolderByIdReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class DeleteFolderByIdTests {

	@Test
	public void testOperationDeleteFolderByIdBasicMapping()  {
		GoogledriveServiceDefaultImpl serviceDefaultImpl = new GoogledriveServiceDefaultImpl();
		DeleteFolderByIdInputParametersDTO inputs = new DeleteFolderByIdInputParametersDTO();
		inputs.setAuthorization(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setId(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setEmptyTrash(false);
		DeleteFolderByIdReturnDTO returnValue = serviceDefaultImpl.deleteFolderById(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}