package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Googledrive.GoogledriveServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.CreateFileInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.CreateFileReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class CreateFileTests {

	@Test
	public void testOperationCreateFileBasicMapping()  {
		GoogledriveServiceDefaultImpl serviceDefaultImpl = new GoogledriveServiceDefaultImpl();
		CreateFileInputParametersDTO inputs = new CreateFileInputParametersDTO();
		inputs.setAuthorization(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setFile(null);
		inputs.setPath(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setSize(0);
		inputs.setTags(new ArrayList&lt;String&gt;());
		inputs.setDescription(null);
		inputs.setOverwrite(false);
		CreateFileReturnDTO returnValue = serviceDefaultImpl.createFile(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponseWrapper200());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}