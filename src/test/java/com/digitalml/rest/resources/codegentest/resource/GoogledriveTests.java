package com.digitalml.rest.resources.codegentest.resource;

import java.security.Principal;

import org.apache.commons.collections.CollectionUtils;

import org.junit.Assert;
import org.junit.Test;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

public class GoogledriveTests {

	@Test
	public void testResourceInitialisation() {
		GoogledriveResource resource = new GoogledriveResource();
		Assert.assertNotNull(resource);
	}

	@Test
	public void testOperationDeleteFolderByIdNoSecurity() {
		GoogledriveResource resource = new GoogledriveResource();
		resource.setSecurityContext(null);

		Response response = resource.deleteFolderById(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, false);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationGetFoldersContentsNoSecurity() {
		GoogledriveResource resource = new GoogledriveResource();
		resource.setSecurityContext(null);

		Response response = resource.getFoldersContents(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, false, 0, 0);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationGetFilesLinksNoSecurity() {
		GoogledriveResource resource = new GoogledriveResource();
		resource.setSecurityContext(null);

		Response response = resource.getFilesLinks(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationGetSearchNoSecurity() {
		GoogledriveResource resource = new GoogledriveResource();
		resource.setSecurityContext(null);

		Response response = resource.getSearch(org.apache.commons.lang3.StringUtils.EMPTY, null, null, null, null, new ArrayList&lt;String&gt;(), 0, 0);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationCreateFileCopyNoSecurity() {
		GoogledriveResource resource = new GoogledriveResource();
		resource.setSecurityContext(null);

		Response response = resource.createFileCopy(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, new CloudFile());
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationCreateFolderCopyNoSecurity() {
		GoogledriveResource resource = new GoogledriveResource();
		resource.setSecurityContext(null);

		Response response = resource.createFolderCopy(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, new CloudFile());
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationGetFilesNoSecurity() {
		GoogledriveResource resource = new GoogledriveResource();
		resource.setSecurityContext(null);

		Response response = resource.getFiles(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationCreateFileNoSecurity() {
		GoogledriveResource resource = new GoogledriveResource();
		resource.setSecurityContext(null);

		Response response = resource.createFile(org.apache.commons.lang3.StringUtils.EMPTY, null, org.apache.commons.lang3.StringUtils.EMPTY, 0, new ArrayList&lt;String&gt;(), null, false);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationDeleteFilesNoSecurity() {
		GoogledriveResource resource = new GoogledriveResource();
		resource.setSecurityContext(null);

		Response response = resource.deleteFiles(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, false);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationCreateFolderNoSecurity() {
		GoogledriveResource resource = new GoogledriveResource();
		resource.setSecurityContext(null);

		Response response = resource.createFolder(org.apache.commons.lang3.StringUtils.EMPTY, new CloudFile());
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationDeleteFoldersNoSecurity() {
		GoogledriveResource resource = new GoogledriveResource();
		resource.setSecurityContext(null);

		Response response = resource.deleteFolders(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, false);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationGetPingNoSecurity() {
		GoogledriveResource resource = new GoogledriveResource();
		resource.setSecurityContext(null);

		Response response = resource.getPing(org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationGetStorageNoSecurity() {
		GoogledriveResource resource = new GoogledriveResource();
		resource.setSecurityContext(null);

		Response response = resource.getStorage(org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationGetFoldersContentsNoSecurity() {
		GoogledriveResource resource = new GoogledriveResource();
		resource.setSecurityContext(null);

		Response response = resource.getFoldersContents(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, false, 0, 0);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationGetFoldersMetadataNoSecurity() {
		GoogledriveResource resource = new GoogledriveResource();
		resource.setSecurityContext(null);

		Response response = resource.getFoldersMetadata(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationUpdateFoldersMetadataNoSecurity() {
		GoogledriveResource resource = new GoogledriveResource();
		resource.setSecurityContext(null);

		Response response = resource.updateFoldersMetadata(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, new Metadata());
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationGetFilesLinksNoSecurity() {
		GoogledriveResource resource = new GoogledriveResource();
		resource.setSecurityContext(null);

		Response response = resource.getFilesLinks(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationUpdateFilesMetadataPropertiesNoSecurity() {
		GoogledriveResource resource = new GoogledriveResource();
		resource.setSecurityContext(null);

		Response response = resource.updateFilesMetadataProperties(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, new CustomProperties());
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationCreateFileCopyNoSecurity() {
		GoogledriveResource resource = new GoogledriveResource();
		resource.setSecurityContext(null);

		Response response = resource.createFileCopy(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, new CloudFile());
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationGetFoldersMetadataNoSecurity() {
		GoogledriveResource resource = new GoogledriveResource();
		resource.setSecurityContext(null);

		Response response = resource.getFoldersMetadata(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationUpdateFoldersMetadataNoSecurity() {
		GoogledriveResource resource = new GoogledriveResource();
		resource.setSecurityContext(null);

		Response response = resource.updateFoldersMetadata(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, new Metadata());
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationCreateFolderCopyNoSecurity() {
		GoogledriveResource resource = new GoogledriveResource();
		resource.setSecurityContext(null);

		Response response = resource.createFolderCopy(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, new CloudFile());
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationGetFilesMetadataNoSecurity() {
		GoogledriveResource resource = new GoogledriveResource();
		resource.setSecurityContext(null);

		Response response = resource.getFilesMetadata(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationUpdateFilesMetadataNoSecurity() {
		GoogledriveResource resource = new GoogledriveResource();
		resource.setSecurityContext(null);

		Response response = resource.updateFilesMetadata(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, new Metadata());
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationGetFileByIdNoSecurity() {
		GoogledriveResource resource = new GoogledriveResource();
		resource.setSecurityContext(null);

		Response response = resource.getFileById(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationDeleteFileByIdNoSecurity() {
		GoogledriveResource resource = new GoogledriveResource();
		resource.setSecurityContext(null);

		Response response = resource.deleteFileById(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, false);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationGetFilesMetadataNoSecurity() {
		GoogledriveResource resource = new GoogledriveResource();
		resource.setSecurityContext(null);

		Response response = resource.getFilesMetadata(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationUpdateFilesMetadataNoSecurity() {
		GoogledriveResource resource = new GoogledriveResource();
		resource.setSecurityContext(null);

		Response response = resource.updateFilesMetadata(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, new Metadata());
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationUpdateFilesMetadataPropertiesNoSecurity() {
		GoogledriveResource resource = new GoogledriveResource();
		resource.setSecurityContext(null);

		Response response = resource.updateFilesMetadataProperties(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, new CustomProperties());
		Assert.assertEquals(403, response.getStatus());
	}


	private SecurityContext unautheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return false;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};

}