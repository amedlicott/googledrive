package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Googledrive.GoogledriveServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.GetFilesMetadataInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.GetFilesMetadataReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class GetFilesMetadataTests {

	@Test
	public void testOperationGetFilesMetadataBasicMapping()  {
		GoogledriveServiceDefaultImpl serviceDefaultImpl = new GoogledriveServiceDefaultImpl();
		GetFilesMetadataInputParametersDTO inputs = new GetFilesMetadataInputParametersDTO();
		inputs.setAuthorization(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setPath(org.apache.commons.lang3.StringUtils.EMPTY);
		GetFilesMetadataReturnDTO returnValue = serviceDefaultImpl.getFilesMetadata(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponseWrapper200());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}