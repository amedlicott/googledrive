package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Googledrive.GoogledriveServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.GetFoldersMetadataInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.GoogledriveService.GetFoldersMetadataReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class GetFoldersMetadataTests {

	@Test
	public void testOperationGetFoldersMetadataBasicMapping()  {
		GoogledriveServiceDefaultImpl serviceDefaultImpl = new GoogledriveServiceDefaultImpl();
		GetFoldersMetadataInputParametersDTO inputs = new GetFoldersMetadataInputParametersDTO();
		inputs.setAuthorization(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setPath(org.apache.commons.lang3.StringUtils.EMPTY);
		GetFoldersMetadataReturnDTO returnValue = serviceDefaultImpl.getFoldersMetadata(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponseWrapper200());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}